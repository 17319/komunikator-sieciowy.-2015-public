﻿using System;

namespace BK_IM_S
{
    [Serializable]
    public class Uzytkownik

    {
        public string nazwaUzytkownika;
        public string hasloUzytkownika;
        [NonSerialized] public bool czyUzytkownikJestZalogowanyIPodlaczony;
        [NonSerialized] public Klient poloczenieInformacja;
        
        public Uzytkownik(string uzytkownik, string haslo)
        {
            this.nazwaUzytkownika = uzytkownik;
            this.hasloUzytkownika = haslo;
            this.czyUzytkownikJestZalogowanyIPodlaczony = false;
        }
        public Uzytkownik(string uzytkownik, string haslo, Klient polaczenie)
        {
            this.nazwaUzytkownika = uzytkownik;
            this.hasloUzytkownika = haslo;
            this.czyUzytkownikJestZalogowanyIPodlaczony = true;
            this.poloczenieInformacja = polaczenie;
        }
    }
}
