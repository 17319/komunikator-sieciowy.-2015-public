﻿using System;
using System.IO;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Text;
using System.Threading;

namespace BK_IM_S
{
    public class Klient
    {
        public Klient(Program program, TcpClient tcpClient)
        {
            prog = program;
            klient = tcpClient;
            (new Thread(new ThreadStart(KonfigurujPolaczenie))).Start();
        }

        Program prog;
        public TcpClient klient;
        public NetworkStream networkStream;
        public SslStream sslStream;
        public BinaryReader binaryReader;
        public BinaryWriter binaryWriter;

        Uzytkownik daneUzytkownika;
        
        void KonfigurujPolaczenie() 
        {
            try
            {
                networkStream = klient.GetStream();
                sslStream = new SslStream(networkStream, false);
                sslStream.AuthenticateAsServer(prog.certyfikat, false, SslProtocols.Tls, true);
                Console.WriteLine("Połączenie uwierzytelniono");

                binaryReader = new BinaryReader(sslStream, Encoding.UTF8);
                binaryWriter = new BinaryWriter(sslStream, Encoding.UTF8);

                binaryWriter.Write(IM_Hello);
                binaryWriter.Flush();
                int hello = binaryReader.ReadInt32();
                if (hello == IM_Hello)
                {
                   
                  byte trybLogowania = binaryReader.ReadByte();
                string nazwaUzyt = binaryReader.ReadString();
                string hasloUzyt = binaryReader.ReadString();

                if (trybLogowania == IM_Register)
                {
                    if (!prog.slownikUzytkownikow.ContainsKey(nazwaUzyt))
                    {
                        daneUzytkownika = new Uzytkownik(nazwaUzyt, hasloUzyt, this);
                        prog.slownikUzytkownikow.Add(nazwaUzyt, daneUzytkownika);
                        binaryWriter.Write(IM_OK);
                        binaryWriter.Flush();
                        Console.WriteLine("Zarejestrowano uzytkownika:  " +  nazwaUzyt);
                        prog.ZapiszUzytkownikow();
                        Odbieraj();
                    }
                    else
                        binaryWriter.Write(IM_Exists);
                }
                else if (trybLogowania == IM_Login)
                {
                    if (prog.slownikUzytkownikow.TryGetValue(nazwaUzyt, out daneUzytkownika))
                    {
                        if (hasloUzyt == daneUzytkownika.hasloUzytkownika)
                        {
                            if (daneUzytkownika.czyUzytkownikJestZalogowanyIPodlaczony)
                                daneUzytkownika.poloczenieInformacja.ZakonczPolaczenie();

                            daneUzytkownika.poloczenieInformacja = this;
                            binaryWriter.Write(IM_OK);
                            binaryWriter.Flush();
                            Odbieraj();
                        }
                        else
                            binaryWriter.Write(IM_WrongPass);
                    }
                    else
                        binaryWriter.Write(IM_NoExists);
                }

            }
                ZakonczPolaczenie();
            }
            catch { ZakonczPolaczenie(); }
        }
        void ZakonczPolaczenie() 
        {
            try
            {
                daneUzytkownika.czyUzytkownikJestZalogowanyIPodlaczony = false;
                binaryReader.Close();
                binaryWriter.Close();
                sslStream.Close();
                networkStream.Close();
                klient.Close();
                Console.WriteLine("Zakonczono polaczenie");
            }
            catch { }
        }
        void Odbieraj()
        {
            Console.WriteLine("Uzytkownik zalogowany " + daneUzytkownika.nazwaUzytkownika);
            daneUzytkownika.czyUzytkownikJestZalogowanyIPodlaczony = true;
            try
            {
                while (klient.Client.Connected)
                {
                    byte type = binaryReader.ReadByte();

                    if (type == IM_IsAvailable)
                    {
                        string who = binaryReader.ReadString();

                        binaryWriter.Write(IM_IsAvailable);
                        binaryWriter.Write(who);

                        Uzytkownik info;
                        if (prog.slownikUzytkownikow.TryGetValue(who, out info))
                        {
                            if (info.czyUzytkownikJestZalogowanyIPodlaczony)
                                binaryWriter.Write(true); 
                            else
                                binaryWriter.Write(false); 
                        }
                        else
                            binaryWriter.Write(false);
                        binaryWriter.Flush();
                    }
                    else if (type == IM_Send)
                    {
                        string adresat = binaryReader.ReadString();
                        string wiadomosc = binaryReader.ReadString();

                        Uzytkownik odbiorca;
                        if (prog.slownikUzytkownikow.TryGetValue(adresat, out odbiorca))
                        {
                            if (odbiorca.czyUzytkownikJestZalogowanyIPodlaczony)
                            {
                                odbiorca.poloczenieInformacja.binaryWriter.Write(IM_Received);
                                odbiorca.poloczenieInformacja.binaryWriter.Write(daneUzytkownika.nazwaUzytkownika);  // From
                                odbiorca.poloczenieInformacja.binaryWriter.Write(wiadomosc);
                                odbiorca.poloczenieInformacja.binaryWriter.Flush();
                                Console.WriteLine("Wysłano wiadomość od " +   daneUzytkownika.nazwaUzytkownika + " do " + odbiorca.nazwaUzytkownika);
                            }
                        }
                    }
                }
            }
            catch (IOException) { }

            daneUzytkownika.czyUzytkownikJestZalogowanyIPodlaczony = false;
            Console.WriteLine("Uzytkownik " + daneUzytkownika.nazwaUzytkownika + "jest wylogowany");
        }

        public const int IM_Hello = 2016;
        public const byte IM_OK = 0;  
        public const byte IM_Login = 1; 
        public const byte IM_Register = 2;
        public const byte IM_Exists = 5;   
        public const byte IM_NoExists = 6;  
        public const byte IM_WrongPass = 7;  
        public const byte IM_IsAvailable = 8; 
        public const byte IM_Send = 9; 
        public const byte IM_Received = 10; 
    }
}
