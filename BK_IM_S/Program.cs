﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography.X509Certificates;

namespace BK_IM_S
{
    public class Program
    {
        static void Main(string[] args)
        {
            Program p = new Program();
            Console.WriteLine();
            Console.ReadLine();
        }

        public X509Certificate2 certyfikat = new X509Certificate2("server.pfx", "instant");
   
        public IPAddress ip = IPAddress.Parse("127.0.0.1");
        public int port = 2000;
        public bool czyDziala = true;
        public TcpListener tcpListener;

        public Dictionary<string, Uzytkownik> slownikUzytkownikow = new Dictionary<string, Uzytkownik>();

        public Program()
        {
            ZalogujUzytkownika();
            tcpListener = new TcpListener(ip, port);
            tcpListener.Start();
            Console.WriteLine(" Server działa OK");
            Nasluch();
        }

        void Nasluch() 
        {
            while (czyDziala)
            {
                TcpClient tcpClient = tcpListener.AcceptTcpClient();
                Klient client = new Klient(this, tcpClient);
            }
        }

        string nazwaUzytkownika = Environment.CurrentDirectory + "\\users.dat";
        public void ZapiszUzytkownikow()  
        {
            try
            {
                Console.WriteLine("Zapisywanie uzytkowników");
                BinaryFormatter bf = new BinaryFormatter();
                FileStream fs = new FileStream(nazwaUzytkownika, FileMode.Create, FileAccess.Write);
                bf.Serialize(fs, slownikUzytkownikow.Values.ToArray());
                fs.Close();
                Console.WriteLine("Zapisano uzytkownika");
            }
            catch (Exception)
            {
                Console.WriteLine("Pojawił się błąd, przy zapisywaniu użytkownika");
            }
        }
        public void ZalogujUzytkownika() 
        {
            try
            {
                Console.WriteLine("Logowanie...");
                BinaryFormatter bf = new BinaryFormatter();
                FileStream fs = new FileStream(nazwaUzytkownika, FileMode.Open, FileAccess.Read);
                Uzytkownik[] ui = (Uzytkownik[])bf.Deserialize(fs);
                fs.Close();
                slownikUzytkownikow = ui.ToDictionary((u) => u.nazwaUzytkownika, (u) => u);
                Console.WriteLine("Uzytkownicy zalogowani w liczbie: " + slownikUzytkownikow.Count);
            }
            catch {        
            }
        }
    }
}
