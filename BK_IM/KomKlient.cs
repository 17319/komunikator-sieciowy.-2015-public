﻿using System;
using System.Text;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace BK_IM
{
    public class KomKlient
    {
        Thread tcpWatekOdbiornik;
        bool czyPoloczono = false;
        bool czyZalogowano = false; 
        string nazwaUzytkownika;          
        string hasloUzytkownika;          
        bool trybRejestracji;              

        public string Server { get { return "localhost"; } }
        public int Port { get { return 2000; } }

        public bool CzyZalogowano { get { return czyZalogowano; } }
        public string NazwaUzytkownika { get { return nazwaUzytkownika; } }
        public string HasloUzytkownika { get { return hasloUzytkownika; } }

        void polaczenie(string uzytkownik, string haslo, bool trybRejestracji)
        {
            if (!czyPoloczono)
            {
                czyPoloczono = true;
                nazwaUzytkownika = uzytkownik;
                hasloUzytkownika = haslo;
                this.trybRejestracji = trybRejestracji;
                tcpWatekOdbiornik = new Thread(new ThreadStart(UstawieniePoloczenia));
                tcpWatekOdbiornik.Start();
            }
        }
        public void Logowanie(string uzytkownik, string haslo)
        {
            polaczenie(uzytkownik, haslo, false);
        }
        public void Rejestracja(string uzytkownik, string haslo)
        {
            polaczenie(uzytkownik, haslo, true);
        }
        public void Rozlaczenie()
        {
            if (czyPoloczono)
                ZakonczPolaczenie();
        }

        public void CzyUzytkownikJestDostepny(string uzytkownik)
        {
            if (czyPoloczono)
            {
                binaryWriter.Write(IM_IsAvailable);
                binaryWriter.Write(uzytkownik);
                binaryWriter.Flush();
            }
        }
        public void SendMessage(string adrestWiadomosci, string wiadomosc)
        {
            if (czyPoloczono)
            {
                binaryWriter.Write(IM_Send);
                binaryWriter.Write(adrestWiadomosci);
                binaryWriter.Write(wiadomosc);
                binaryWriter.Flush();
            }
        }

        // Events
        public event EventHandler LoginOK;
        public event EventHandler RejestracjaOK;
        public event KOMBledyEH BlednyLogin;
        public event KOMBledyEH RejestracjaNieUDana;
        public event EventHandler Rozlaczono;
        public event KOMDostepnoscEH UzytkownikDostepny;
        public event KOMOgolEH WiadomoscOtrzymana;

        virtual protected void LLoginOK()
        {
            if (LoginOK != null)
                LoginOK(this, EventArgs.Empty);
        }
        virtual protected void RRejestracjaOK()
        {
            if (RejestracjaOK != null)
                RejestracjaOK(this, EventArgs.Empty);
        }
        virtual protected void BBlednyLogin(KOMBledyEA e)
        {
            if (BlednyLogin != null)
                BlednyLogin(this, e);
        }
        virtual protected void RRejestracjaNieUDana(KOMBledyEA e)
        {
            if (RejestracjaNieUDana != null)
                RejestracjaNieUDana(this, e);
        }
        virtual protected void RRozlaczono()
        {
            if (Rozlaczono != null)
                Rozlaczono(this, EventArgs.Empty);
        }
        virtual protected void UUzytkownikDostepny(KOMDostepnoscEA e)
        {
            if (UzytkownikDostepny != null)
                UzytkownikDostepny(this, e);
        }
        virtual protected void WWiadomoscOtrzymana(KOMOtrzyEA e)
        {
            if (WiadomoscOtrzymana != null)
                WiadomoscOtrzymana(this, e);
        }

        
        TcpClient tcpClient;
        NetworkStream networkStream;
        SslStream sslStream;
        BinaryReader binaryReader;
        BinaryWriter binaryWriter;

        void UstawieniePoloczenia() 
        {
            tcpClient = new TcpClient(Server, Port); 
            networkStream = tcpClient.GetStream();
            sslStream = new SslStream(networkStream, false, new RemoteCertificateValidationCallback(sprawdzCertyfikat));
            sslStream.AuthenticateAsClient("InstantMessengerServer");

            binaryReader = new BinaryReader(sslStream, Encoding.UTF8);
            binaryWriter = new BinaryWriter(sslStream, Encoding.UTF8);

            int hello = binaryReader.ReadInt32();
            if (hello == IM_Hello)
            {
                binaryWriter.Write(IM_Hello);
                binaryWriter.Write(trybRejestracji ? IM_Register : IM_Login);
                binaryWriter.Write(NazwaUzytkownika);
                binaryWriter.Write(HasloUzytkownika);
                binaryWriter.Flush();

                byte odpowiedz = binaryReader.ReadByte(); 
                if (odpowiedz == IM_OK) 
                {
                    if (trybRejestracji)
                        RRejestracjaOK();  
                    LLoginOK();  
                    Nasluchiwanie(); 
                }
                else
                {
                    KOMBledyEA blad = new KOMBledyEA((KomBledyEnum)odpowiedz);
                    if (trybRejestracji)
                        RRejestracjaNieUDana(blad);
                    else
                        BBlednyLogin(blad);
                }
            }
            if (czyPoloczono)
                ZakonczPolaczenie();
        }
        void ZakonczPolaczenie() 
        {
            binaryReader.Close();
            binaryWriter.Close();
            sslStream.Close();
            networkStream.Close();
            tcpClient.Close();
            RRozlaczono();
            czyPoloczono = false;
        }
        void Nasluchiwanie()
        {
            czyZalogowano = true;
            try
            {
                while (tcpClient.Connected) 
                {
                    byte typ = binaryReader.ReadByte(); 

                    if (typ == IM_IsAvailable)
                    {
                        string uzytkownik = binaryReader.ReadString();
                        bool czyJestDostepny = binaryReader.ReadBoolean();
                        UUzytkownikDostepny(new KOMDostepnoscEA(uzytkownik, czyJestDostepny));
                    }
                    else if (typ == IM_Received)
                    {
                        string odKogo = binaryReader.ReadString();
                        string trescWiadomosci = binaryReader.ReadString();
                        WWiadomoscOtrzymana(new KOMOtrzyEA(odKogo, trescWiadomosci));
                    }
                }
            }
            catch (IOException) { }

            czyZalogowano = false;
        }

        public const int IM_Hello = 2016;
        public const byte IM_OK = 0; 
        public const byte IM_Login = 1; 
        public const byte IM_Register = 2;  
        public const byte IM_TooUsername = 3; 
        public const byte IM_TooPassword = 4;  
        public const byte IM_Exists = 5;     
        public const byte IM_NoExists = 6;   
        public const byte IM_WrongPass = 7;   
        public const byte IM_IsAvailable = 8;  
        public const byte IM_Send = 9;   
        public const byte IM_Received = 10;  
        
        public static bool sprawdzCertyfikat(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {

            return true;
        }
    }
}
