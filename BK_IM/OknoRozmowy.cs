﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BK_IM
{
    public partial class OknoRozmowy : Form
    {
        

        public KomKlient komKlient;
        public string adtresatWiadomosci;
        public OknoRozmowy(KomKlient komKlient, string nazwaUzytkownika)
        {
            InitializeComponent();
            this.komKlient = komKlient;
            this.adtresatWiadomosci = nazwaUzytkownika;
        }
        KOMDostepnoscEH komDostepnoscEH;
        KOMOgolEH komOgolEH;
        private void TalkForm_Load(object sender, EventArgs e)
        {
            this.Text = adtresatWiadomosci;
            komDostepnoscEH = new KOMDostepnoscEH(KOMUzytkownikDostepny);
            komOgolEH = new KOMOgolEH(im_MessageReceived);
            komKlient.UzytkownikDostepny += komDostepnoscEH;
            komKlient.WiadomoscOtrzymana += komOgolEH;
            komKlient.CzyUzytkownikJestDostepny(adtresatWiadomosci);
        }
        private void TalkForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            komKlient.UzytkownikDostepny -= komDostepnoscEH;
            komKlient.WiadomoscOtrzymana -= komOgolEH;
        }

        private void sendButton_Click(object sender, EventArgs e)
        {
            komKlient.SendMessage(adtresatWiadomosci, txtNapiszWiadomosc.Text);
            txtRozmowa.Text += String.Format("[{0}] {1}\r\n", komKlient.NazwaUzytkownika, txtNapiszWiadomosc.Text);
            txtNapiszWiadomosc.Text = "";
        }

        bool ostatnioDostepny = false;
        void KOMUzytkownikDostepny(object sender, KOMDostepnoscEA e)
        {
            this.BeginInvoke(new MethodInvoker(delegate
            {
                if (e.NazwaUzykownika == adtresatWiadomosci)
                {
                    if (ostatnioDostepny != e.czyDostepny)
                    {
                        ostatnioDostepny = e.czyDostepny;
                        string dostepny = (e.czyDostepny ? "dostepny" : "niedostepny");
                        this.Text = String.Format("{0} - {1}", adtresatWiadomosci, dostepny);
                        txtRozmowa.Text += string.Format(adtresatWiadomosci + " jest " +  dostepny);
                    }
                }
            }));
        }
        void im_MessageReceived(object sender, KOMOtrzyEA e)
        {
            this.BeginInvoke(new MethodInvoker(delegate
            {
                if (e.Skad == adtresatWiadomosci)
                {
                    txtRozmowa.Text += String.Format("[{0}] {1}\r\n", e.Skad, e.Wiadomosc);
                }
            }));
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            komKlient.CzyUzytkownikJestDostepny(adtresatWiadomosci);
        }

        private void sendText_Enter(object sender, EventArgs e)
        {
            komKlient.SendMessage(adtresatWiadomosci, txtNapiszWiadomosc.Text);
            txtRozmowa.Text += String.Format("[{0}] {1}\r\n", komKlient.NazwaUzytkownika, txtNapiszWiadomosc.Text);
            txtNapiszWiadomosc.Text = "";
        }
    }
}
