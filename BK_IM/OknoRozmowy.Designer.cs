﻿namespace BK_IM
{
    partial class OknoRozmowy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.txtRozmowa = new System.Windows.Forms.TextBox();
            this.txtNapiszWiadomosc = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.tntWyslij = new System.Windows.Forms.ToolStripMenuItem();
            this.timer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.txtRozmowa);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.txtNapiszWiadomosc);
            this.splitContainer1.Panel2.Controls.Add(this.menuStrip1);
            this.splitContainer1.Size = new System.Drawing.Size(584, 562);
            this.splitContainer1.SplitterDistance = 433;
            this.splitContainer1.TabIndex = 0;
            // 
            // txtRozmowa
            // 
            this.txtRozmowa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRozmowa.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtRozmowa.Location = new System.Drawing.Point(0, 0);
            this.txtRozmowa.Multiline = true;
            this.txtRozmowa.Name = "txtRozmowa";
            this.txtRozmowa.ReadOnly = true;
            this.txtRozmowa.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRozmowa.Size = new System.Drawing.Size(580, 429);
            this.txtRozmowa.TabIndex = 2;
            // 
            // txtNapiszWiadomosc
            // 
            this.txtNapiszWiadomosc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNapiszWiadomosc.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtNapiszWiadomosc.Location = new System.Drawing.Point(0, 0);
            this.txtNapiszWiadomosc.Multiline = true;
            this.txtNapiszWiadomosc.Name = "txtNapiszWiadomosc";
            this.txtNapiszWiadomosc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtNapiszWiadomosc.Size = new System.Drawing.Size(580, 97);
            this.txtNapiszWiadomosc.TabIndex = 1;
            this.txtNapiszWiadomosc.Enter += new System.EventHandler(this.sendText_Enter);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tntWyslij});
            this.menuStrip1.Location = new System.Drawing.Point(0, 97);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(580, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // tntWyslij
            // 
            this.tntWyslij.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tntWyslij.Name = "tntWyslij";
            this.tntWyslij.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tntWyslij.Size = new System.Drawing.Size(50, 20);
            this.tntWyslij.Text = "Wyślij";
            this.tntWyslij.Click += new System.EventHandler(this.sendButton_Click);
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Interval = 10000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // OknoRozmowy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 562);
            this.Controls.Add(this.splitContainer1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "OknoRozmowy";
            this.Text = "Okno rozmowy";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TalkForm_FormClosing);
            this.Load += new System.EventHandler(this.TalkForm_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TextBox txtNapiszWiadomosc;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tntWyslij;
        private System.Windows.Forms.TextBox txtRozmowa;
        private System.Windows.Forms.Timer timer;
    }
}