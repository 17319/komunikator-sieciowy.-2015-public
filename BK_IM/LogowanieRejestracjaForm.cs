﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BK_IM
{
    public partial class RejestracjaLogowanie : Form
    {
        public RejestracjaLogowanie()
        {
            InitializeComponent();
        }

        public string NazwaUzytkownika;
        public string hasloUzytkownika;

        private void okButton_Click(object sender, EventArgs e)
        {
            NazwaUzytkownika = txtNazwaUzytkownika.Text;
            hasloUzytkownika = txtHaslo.Text;
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
