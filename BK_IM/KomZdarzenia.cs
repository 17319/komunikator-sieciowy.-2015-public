﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BK_IM
{
    public enum KomBledyEnum : byte
    {
        ZbytDlugaNazwaUzytkownika = KomKlient.IM_TooUsername,
        ZbytDlugieHaslo = KomKlient.IM_TooPassword,
        UzytkownikIstnieje = KomKlient.IM_Exists,
        UzytkownikNieIstnieje = KomKlient.IM_NoExists,
        ZleHaslo = KomKlient.IM_WrongPass
    }

    public class KOMBledyEA : EventArgs
    {
        KomBledyEnum blad;

        public KOMBledyEA(KomBledyEnum blad)
        {
            this.blad = blad;
        }

        public KomBledyEnum Error
        {
            get { return blad; }
        }
    }
    public class KOMDostepnoscEA : EventArgs
    {
        string uzykownik;
        bool haslo;

        public KOMDostepnoscEA(string user, bool avail)
        {
            this.uzykownik = user;
            this.haslo = avail;
        }

        public string NazwaUzykownika
        {
            get { return uzykownik; }
        }
        public bool czyDostepny
        {
            get { return haslo; }
        }
    }
    public class KOMOtrzyEA : EventArgs
    {
        string uzytkownik;
        string wiadomosc;

        public KOMOtrzyEA(string uzytkownik, string wiadomosc)
        {
            this.uzytkownik = uzytkownik;
            this.wiadomosc = wiadomosc;
        }

        public string Skad
        {
            get { return uzytkownik; }
        }
        public string Wiadomosc
        {
            get { return wiadomosc; }
        }
    }

    public delegate void KOMBledyEH(object sender, KOMBledyEA e);
    public delegate void KOMDostepnoscEH(object sender, KOMDostepnoscEA e);
    public delegate void KOMOgolEH(object sender, KOMOtrzyEA e);
}
