﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace BK_IM
{
    public partial class Form1 : Form
    {
        KomKlient im = new KomKlient();

        public Form1()
        {
            InitializeComponent();
            im.LoginOK += new EventHandler(KOM_Login_OK);
            im.RejestracjaOK += new EventHandler(KOM_Rejestracja_OK);
            im.BlednyLogin += new KOMBledyEH(KOM_Bledny_Login);
            im.RejestracjaNieUDana += new KOMBledyEH(KOM_Nieudana_Rejestracja);
            im.Rozlaczono += new EventHandler(KOM_Rozlaczono);
        }
 
        void KOM_Login_OK(object sender, EventArgs e)
        {
            this.BeginInvoke(new MethodInvoker(delegate
            {
                TSSLInformacja.Text = "Zalogowano";
                tntMow.Enabled = true;
            }));
        }
        void KOM_Rejestracja_OK(object sender, EventArgs e)
        {
            this.BeginInvoke(new MethodInvoker(delegate
            {
                TSSLInformacja.Text = "Zarejestrowano!";
            }));
        }
        void KOM_Bledny_Login(object sender, KOMBledyEA e)
        {
            this.BeginInvoke(new MethodInvoker(delegate
            {
                TSSLInformacja.Text = "Błędny login";
            }));
        }
        void KOM_Nieudana_Rejestracja(object sender, KOMBledyEA e)
        {
            this.BeginInvoke(new MethodInvoker(delegate
            {
                TSSLInformacja.Text = "Rejestracja nieudana";
            }));
        }
        void KOM_Rozlaczono(object sender, EventArgs e)
        {
            this.BeginInvoke(new MethodInvoker(delegate
            {
                TSSLInformacja.Text = "Rozłączono";

                foreach (OknoRozmowy tf in listaRozmow)
                    tf.Close();
            }));
        }

        List<OknoRozmowy> listaRozmow = new List<OknoRozmowy>();
        private void talkButton_Click(object sender, EventArgs e)
        {
            OknoRozmowy formularzRozmowy = new OknoRozmowy(im, txtAdresat.Text);
            txtAdresat.Text = "";
            listaRozmow.Add(formularzRozmowy);
            formularzRozmowy.Show();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            im.Rozlaczenie();
        }

        private void buttonZarejestruj_Click(object sender, EventArgs e)
        {
            RejestracjaLogowanie info = new RejestracjaLogowanie();
            if (info.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                im.Rejestracja(info.NazwaUzytkownika, info.hasloUzytkownika);
                TSSLInformacja.Text = "Rejestracja";
            }
        }

        private void buttonZaloguj_Click(object sender, EventArgs e)
        {
            RejestracjaLogowanie info = new RejestracjaLogowanie();
            if (info.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                im.Logowanie(info.NazwaUzytkownika, info.hasloUzytkownika);
                TSSLInformacja.Text = "Logowanie";
            }
        }

        private void buttonWyloguj_Click(object sender, EventArgs e)
        {
            im.Rozlaczenie();
        }
    }
}
