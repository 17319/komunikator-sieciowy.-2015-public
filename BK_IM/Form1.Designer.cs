﻿namespace BK_IM
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.TSSLInformacja = new System.Windows.Forms.ToolStripStatusLabel();
            this.txtAdresat = new System.Windows.Forms.TextBox();
            this.tntMow = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonZarejestruj = new System.Windows.Forms.Button();
            this.buttonZaloguj = new System.Windows.Forms.Button();
            this.buttonWyloguj = new System.Windows.Forms.Button();
            this.statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip
            // 
            this.statusStrip.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSSLInformacja});
            this.statusStrip.Location = new System.Drawing.Point(0, 103);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(656, 30);
            this.statusStrip.TabIndex = 0;
            this.statusStrip.Text = "statusStrip1";
            // 
            // TSSLInformacja
            // 
            this.TSSLInformacja.Name = "TSSLInformacja";
            this.TSSLInformacja.Size = new System.Drawing.Size(211, 25);
            this.TSSLInformacja.Text = "Zarejestruj albo zaloguj";
            // 
            // txtAdresat
            // 
            this.txtAdresat.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAdresat.Location = new System.Drawing.Point(153, 44);
            this.txtAdresat.Name = "txtAdresat";
            this.txtAdresat.Size = new System.Drawing.Size(491, 20);
            this.txtAdresat.TabIndex = 2;
            // 
            // tntMow
            // 
            this.tntMow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tntMow.Enabled = false;
            this.tntMow.Location = new System.Drawing.Point(153, 74);
            this.tntMow.Name = "tntMow";
            this.tntMow.Size = new System.Drawing.Size(491, 23);
            this.tntMow.TabIndex = 3;
            this.tntMow.Text = "Rozmowa";
            this.tntMow.UseVisualStyleBackColor = true;
            this.tntMow.Click += new System.EventHandler(this.talkButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(150, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Nazwa użytkownika:";
            // 
            // buttonZarejestruj
            // 
            this.buttonZarejestruj.Location = new System.Drawing.Point(0, 12);
            this.buttonZarejestruj.Name = "buttonZarejestruj";
            this.buttonZarejestruj.Size = new System.Drawing.Size(131, 23);
            this.buttonZarejestruj.TabIndex = 5;
            this.buttonZarejestruj.Text = "Zarejestruj";
            this.buttonZarejestruj.UseVisualStyleBackColor = true;
            this.buttonZarejestruj.Click += new System.EventHandler(this.buttonZarejestruj_Click);
            // 
            // buttonZaloguj
            // 
            this.buttonZaloguj.Location = new System.Drawing.Point(0, 42);
            this.buttonZaloguj.Name = "buttonZaloguj";
            this.buttonZaloguj.Size = new System.Drawing.Size(131, 23);
            this.buttonZaloguj.TabIndex = 6;
            this.buttonZaloguj.Text = "Zaloguj";
            this.buttonZaloguj.UseVisualStyleBackColor = true;
            this.buttonZaloguj.Click += new System.EventHandler(this.buttonZaloguj_Click);
            // 
            // buttonWyloguj
            // 
            this.buttonWyloguj.Location = new System.Drawing.Point(0, 72);
            this.buttonWyloguj.Name = "buttonWyloguj";
            this.buttonWyloguj.Size = new System.Drawing.Size(131, 23);
            this.buttonWyloguj.TabIndex = 7;
            this.buttonWyloguj.Text = "Wyloguj";
            this.buttonWyloguj.UseVisualStyleBackColor = true;
            this.buttonWyloguj.Click += new System.EventHandler(this.buttonWyloguj_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(656, 133);
            this.Controls.Add(this.buttonWyloguj);
            this.Controls.Add(this.buttonZaloguj);
            this.Controls.Add(this.buttonZarejestruj);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tntMow);
            this.Controls.Add(this.txtAdresat);
            this.Controls.Add(this.statusStrip);
            this.Name = "Form1";
            this.Text = "BK_IM";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel TSSLInformacja;
        private System.Windows.Forms.TextBox txtAdresat;
        private System.Windows.Forms.Button tntMow;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonZarejestruj;
        private System.Windows.Forms.Button buttonZaloguj;
        private System.Windows.Forms.Button buttonWyloguj;
    }
}

